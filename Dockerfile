FROM golang:1.11

WORKDIR /p0
ENV PROD=DOCKER


ADD . .
ARG GO111MODULE=on
RUN go get -v
RUN go install p0

#RUN go get github.com/codegangsta/gin
#RUN go get
#RUN go install

EXPOSE 8080
ENTRYPOINT /go/bin/p0