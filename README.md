Hi welcome to project 0 of this damnable subject.

Running the program: docker-compose up -d --build 
Ports Exposed: 8080 and whatever default ports of Mongodb is 


Update 1) Integrated with Circle CI to do accuracy checking when pushing up
Update 2) docker-compose up works
Update 3) performance testing is up but buggy [esp upload]


TODO: 

Better Content Tests: Tests that require content checking should have content checking, currently we only check the status of the response and the expected status given back. For example, downloading item back should saved the item into files and then check the eTag after the download is over. Also metadata, we can do more testing on metadata to check if we are getting the correct metadata. Also upload hash checking and content length checking, although the server already does it for us, maybe test could also do a double check [or is it triple check in this case].

Better performance testing: Fix upload for locust.py

Better accuracy testing: more test function can be made to do specific testing, current there is only one that test bucket endpoints and another that test the entire flow, i feel that the entire flow test can be splitted up some more and allow for more specific test and general flow can just be general testing or user flow.

Collaborator: Nut and everyone else in class, but especially Nut, also especially his dockerizing and general Golang knowledge