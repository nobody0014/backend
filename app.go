package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	. "p0/config"
	. "p0/dao"
	. "p0/model"
	. "p0/misc"
	"errors"
	"sort"
	"strconv"
	"strings"
	"time"
)

var appconfig = Config{}
var serverconfig = SOSConfig{}
var bucketdao = BucketDAO{}

func CreateBucketEndPoint(w http.ResponseWriter, r *http.Request) {

	bucketname, err := GetBucketName(r)
	if err != nil {
		log.New(os.Stdout,"Error in getting bucketname",0)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	bucket, err := bucketdao.FindByName(bucketname)
	if err != nil {
		if err.Error() != "BucketExist"{
			bucket.ID = bson.NewObjectId()
			bucket.Name = bucketname
			bucket.Objects = [] Object{}
			bucket.Created = GetEpoch()
			bucket.Modified = bucket.Created
			err := bucketdao.Insert(bucket)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}else{
				//try to create new folder, if error, give back internal server error and delete the bucket
				filepath := "./buckets/"+bucketname
				if _, err := os.Stat(bucketname); os.IsNotExist(err) {
					err = os.MkdirAll(filepath, 0755)
					if err != nil {
						bucketdao.Delete(bucket)
						http.Error(w, err.Error(), http.StatusBadRequest)
						log.New(os.Stdout,"Error in making directory",0)
						return
					}
				}
			}
		}else{
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	} else{
		// it probably will never hit here but whatever
		http.Error(w, errors.New("BucketExist").Error(), http.StatusBadRequest)
		return
	}
	// GO BACK AND ASK ABOUT THE CREATED AND MODIFIED
	data := CreateBucketResponse{bucket.Created,bucket.Modified,bucket.Name}
	RespondWithJson(w, http.StatusOK, data)
}


func DropBucketEndPoint(w http.ResponseWriter, r *http.Request) {
	bucketname, err := GetBucketName(r)
	if err != nil && err.Error() != "BucketExist"{
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	filepath := "./buckets/" +bucketname + "/"

	bucket, err := bucketdao.FindByName(bucketname)
	if err != nil && err.Error() == "BucketExist"{
		err = bucketdao.Delete(bucket)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		err = os.RemoveAll(filepath)
		if err != nil{
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}else{
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}



func ListObjectsInBucketEndPoint(w http.ResponseWriter, r *http.Request) {
	bucketname, err := GetBucketName(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	bucket, err := bucketdao.FindByName(bucketname)
	if err != nil && err.Error() != "BucketExist" {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	objs := [] ReturnObject {}

	for _,object := range bucket.Objects {
		if object.CompleteStatus {
			obj := ReturnObject{
				Name: object.Name,
				ETag: object.ETag,
				Created: object.Created,
				Modified: object.Modified,
			}
			objs = append(objs,obj)
		}
	}

	RespondWithJson(w,
		http.StatusOK,
		ListObjectsInBucketResponse{
			bucket.Created,
			bucket.Modified,
			bucket.Name,
			objs})
}


// Parse the configuration file 'config.toml', and establish a connection to DB
func init() {
	appconfig.Read()
	env, f := os.LookupEnv("PROD")
	//println(env,f)
	//serverconfig.Server = appconfig.Productionserver
	//serverconfig.Port = appconfig.Productionserverport
	//serverconfig.Type = "PROD"
	if f && env == "DOCKER" {
		serverconfig.Server = appconfig.Productionserver
		serverconfig.Port = appconfig.Productionserverport
		serverconfig.Type = "PROD"
	}else {
		serverconfig.Server = appconfig.Devserver
		serverconfig.Port = appconfig.Devserverport
		serverconfig.Type = "DEV"
	}
	if f && env == "DOCKER" {
		bucketdao.Server = appconfig.Productiondatabaseserver
		bucketdao.Database = appconfig.Productiondatabase
		bucketdao.Port = appconfig.Productiondatabaseport
	}else {
		bucketdao.Server = appconfig.Devdatabaseserver
		bucketdao.Database = appconfig.Devdatabase
		bucketdao.Port = appconfig.Devdatabaseport
	}
	bucketdao.Connect()
}

// object management
// upload objects
/*
How to create ticket,
1) create tickets for the end points in mongo db
2) if there exist a ticket in mongo db already, return error
*/
func CreateMultiPartsUploadTicketEndPoint(w http.ResponseWriter, r *http.Request){
	bucketname, err := GetBucketName(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	objectname, err := GetObjectName(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	bucket, err := bucketdao.FindByName(bucketname)
	if err.Error() != "BucketExist"{
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	for _, object := range bucket.Objects {
		if object.Name == objectname {
			http.Error(w, errors.New("ObjectExist").Error(), http.StatusBadRequest)
			return
		}
	}

	//// no object found so we can safely create new object
	ctime := GetEpoch()
	nobj := Object{
		ID: bson.NewObjectId(),
		Name: objectname,
		BucketName: bucketname,
		CompleteStatus: false,
		Parts: []Part{},
		ETag: "",
		Size: -1,
		Attributes: [] MetaData {},
		Created: ctime,
		Modified: ctime,
	}

	bucket.Objects = append(bucket.Objects,nobj)
	bucketdao.Update(bucket)
	return
}

/*
How to upload part
Check if part exist
Create that part up - add details
Add Part into Object in that bucket - If part is exists in that object, replace that part
Update the bucket
Write the content of the Part into a file with ending -p1

ID bson.ObjectId `bson:"_id" json:"id"`
ObjectName string `bson:"objectname" json:"objectname"`
BucketName string `bson:"bucketname" json:"bucketname"`
PartNumber int `bson:"partnumber" json:"partnumber"`
UploadStatus bool `bson:"uploadstatus" json:"uploadstatus"`
*/
func UploadObjectPartsEndPoint(w http.ResponseWriter, r *http.Request) {
	log.Print("Uploading Part")
	bucketname,objectname,bucket,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	if err != nil {
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: "",
			Length: -1,
			PartNumber: -1,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}

	if object.CompleteStatus {
		toret := UploadObjectPartsResponse{
			MD5: "eTag:"+object.ETag,
			Length: object.Size,
			PartNumber: -1,
			Error: "CompletedObject",
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	log.Print("Part Number")
	log.Print(r.URL.Query().Encode())
	partnumber, err := strconv.Atoi(r.URL.Query().Get("partNumber"))

	if err != nil{
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: "",
			Length: -1,
			PartNumber: partnumber,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}else if partnumber <= 0 || partnumber > 10000{
		toret := UploadObjectPartsResponse{
			MD5: "",
			Length: -1,
			PartNumber: partnumber,
			Error: "InvalidPartNumber",
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}

	log.Println("Part Size")
	log.Println(r.Header.Get("Content-Length"))
	//log.Println(r.Body)
	partsize, err := strconv.Atoi(r.Header.Get("Content-Length"))
	if err != nil{
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: "",
			Length: partsize,
			PartNumber: partnumber,
			Error: err.Error() + "\nPossibleInvalidContentLength",
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	log.Println("Part_Size:",partsize)

	log.Println("Getting Content-MD5")
	partmd5 := r.Header.Get("Content-MD5")

	log.Print(len(partmd5))
	if len(partmd5) == 0 || len(partmd5) != 32 {
		log.Print("creating obj")
		toret := UploadObjectPartsResponse{
			MD5: "",
			Length: partsize,
			PartNumber: partnumber,
			Error: "InvalidMD5",
		}
		log.Print("responding")
		log.Print(toret)
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	log.Println("Part_MD5:",partmd5)

	log.Print("Getting file path")
	filepath := "./buckets/"+bucketname+"/"+objectname + "-" + strconv.Itoa(partnumber)
	log.Print("Making part")
	npart := Part{
		ID: bson.NewObjectId(),
		ObjectName: objectname,
		BucketName: bucketname,
		PartNumber: partnumber,
		PartName: filepath,
		PartSize: partsize,
		PartMD5: partmd5,
		PartByteStart: -1,
		PartByteEnd: -1,
	}

	// adding part into object pretty easily
	log.Print("Searching object")
	found := false
	for i,part := range object.Parts {
		if part.PartNumber == partnumber {
			object.Parts[i] = npart
			found = true
			break
		}
	}
	if !found {
		object.Parts = append(object.Parts,npart)
	}



	// writing the content of the file into files
	// check if file exists
	log.Print("Create file")
	err = CreateFile(filepath)
	if err != nil {
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}

	defer r.Body.Close()
	log.Print("Opening file")
	//getting file writer and writing into file
	file,err := os.OpenFile(filepath, os.O_RDWR, 0644)

	defer file.Close()
	if err != nil {
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	log.Print("Copying")
	_,err = io.Copy(file,r.Body)
	if err != nil {
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	//file.Close()

	fi,err := file.Stat()
	if err != nil {
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	if int(fi.Size()) != partsize {
		DeleteFile(filepath)
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: "WriteMismatch:FileSize=" + strconv.Itoa(int(fi.Size())),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}

	fmd5,err := HashFileMD5(filepath)
	if err != nil {
		DeleteFile(filepath)
		log.Print("Error: "+err.Error())
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: "MD5CreationERROR",
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	if fmd5 != partmd5 {
		DeleteFile(filepath)
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: "MD5Mismatched:FileMD5="+fmd5,
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}


	// updating bucket and object since we changed the object inside it
	// check if the part exist in the object or not
	ri := -1
	for i,par := range object.Parts  {
		if par.PartNumber == npart.PartNumber {
			ri = i
			break
		}
	}
	if ri != -1 {
		object.Parts[ri] = npart
	}else{
		object.Parts = append(object.Parts,npart)
	}
	object.Modified = GetEpoch()
	err = bucketdao.UpdateObject(bucket,object)
	if err != nil {
		//DeleteFile(filepath)
		toret := UploadObjectPartsResponse{
			MD5: partmd5,
			Length: partsize,
			PartNumber: partnumber,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}

	toret := UploadObjectPartsResponse{
		MD5: npart.PartMD5,
		Length: npart.PartSize,
		PartNumber: npart.PartNumber,
	}
	RespondWithJson(w,http.StatusOK,toret)
}



/*
Show the shit
Ok first
1) object and bucket checking
2) changing the part byte start and end
3) while doing that also keep concatnating
*/
func CompleteMultiPartUploadEndPoint(w http.ResponseWriter, r *http.Request){
	log.Print("Start complete")
	_,_,bucket,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	log.Print("Going in couple of checks")
	if err != nil {
		toret := CompleteObjectResponse{
			ETag: "",
			Length: -1,
			Name: "",
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	log.Print("Other checks")
	if object.CompleteStatus{
		toret := CompleteObjectResponse{
			ETag: object.ETag,
			Length: object.Size,
			Name: object.Name,
			Error: "CompletedObject",
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	if len(object.Parts) == 0 {
		toret := CompleteObjectResponse{
			ETag: object.ETag,
			Length: object.Size,
			Name: object.Name,
			Error: "EmptyObject",
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}


	log.Print("Initializing stuffs")
	currentByte := 0
	concatmd5 := ""
	size := 0
	log.Print("Sort")
	sort.Slice(object.Parts, func(i, j int) bool {
		return object.Parts[i].PartNumber <  object.Parts[j].PartNumber
	})
	log.Print("Clean up parts")
	for i := 0; i < len(object.Parts) ; i++  {
		object.Parts[i].PartByteStart = currentByte
		object.Parts[i].PartByteEnd = currentByte + object.Parts[i].PartSize
		currentByte = currentByte + object.Parts[i].PartSize
		concatmd5 += object.Parts[i].PartMD5
		size += object.Parts[i].PartSize
	}

	log.Print("Empty objects")
	if size == 0 {
		toret := CompleteObjectResponse{
			ETag: "",
			Length: size,
			Name: object.Name,
			Error: "EmptyObject",
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
	}
	log.Print("Making etag")
	hash := md5.New()
	c2,err := hex.DecodeString(concatmd5)
	if err != nil {
		toret := CompleteObjectResponse{
			ETag: "",
			Length: size,
			Name: object.Name,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	log.Print("writing etag")
	_,err = hash.Write(c2)
	if err != nil {
		toret := CompleteObjectResponse{
			ETag: "",
			Length: size,
			Name: object.Name,
			Error: err.Error(),
		}
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}


	//since upload has been completed, we need to check the first file and give it the Content-Type Tag through attributes
	log.Println("Getting content type")
	contentType, err := DetectContentType(object)
	log.Println("Adding Content Type to metadata")
	object.Attributes = append(object.Attributes, MetaData{
		ID: bson.NewObjectId(),
		BucketName: bucket.Name,
		ObjectName: object.Name,
		Key: "Content-Type",
		Value: contentType,
	})


	log.Print("Updating stuffs")
	object.ContentType = contentType
	etag := hex.EncodeToString(hash.Sum(nil)[:16])
	object.ETag = etag + "-" + strconv.Itoa(len(object.Parts))
	object.Size = size
	object.CompleteStatus = true
	object.Modified = GetEpoch()
	toret := CompleteObjectResponse{
		ETag: object.ETag,
		Length: object.Size,
		Name: object.Name,
		Error: "",
	}
	err = bucketdao.UpdateObject(bucket,object)
	if err != nil {
		toret.Error = err.Error()
		RespondWithJson(w,http.StatusBadRequest,toret)
		return
	}
	RespondWithJson(w,http.StatusOK,toret)
	return
}

/*
1) do checking
2) get partnumber - check if that part exist
3) delete the file from it
4) remove part from array
5) update the part through bucket update
*/
func DeletePartEndPoint(w http.ResponseWriter, r *http.Request){
	bucketname,objectname,bucket,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if object.CompleteStatus {
		http.Error(w, "CompletedObject", http.StatusBadRequest)
		return
	}
	partnumber, err := strconv.Atoi(r.URL.Query().Get("partNumber"))
	if err != nil{
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if partnumber <= 0 || partnumber > 10000 {
		http.Error(w, "InvalidPartNumber", http.StatusBadRequest)
		return
	}
	// check if part exist
	found := false
	index := -1
	for i,part := range object.Parts {
		if part.PartNumber == partnumber {
			found = true
			index = i
			break
		}
	}
	if found {
		filepath := "./buckets/"+bucketname+"/"+objectname+"-"+strconv.Itoa(partnumber)
		err := os.Remove(filepath)
		if err != nil {
			http.Error(w, "InvalidFileRemoval", http.StatusBadRequest)
			return
		}
		object.Parts = append(object.Parts[:index],object.Parts[index+1:]...)
		err = bucketdao.UpdateObject(bucket,object)
		if err != nil {
			http.Error(w, "InvalidFileRemoval", http.StatusBadRequest)
			return
		}
	}else{
		return
	}
}

/*
1) do checking
2) get partnumber - check if that part exist
3) delete the file from it
4) remove object from array of objects
5) update the part through bucket update
*/
func DeleteObjectEndPoint(w http.ResponseWriter, r *http.Request){
	bucketname,objectname,bucket,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	found := false
	index := -1
	for i,obj := range bucket.Objects {
		if obj.Name == objectname {
			found = true
			index = i
		}
	}

	if found {
		for _,part := range object.Parts {
			filepath := "./buckets/"+bucketname+"/"+objectname+"-"+strconv.Itoa(part.PartNumber)
			err = DeleteFile(filepath)
			if err != nil &&  !os.IsNotExist(err) {
				if err !=nil {
					log.Print(err.Error())
					http.Error(w, err.Error(), http.StatusBadRequest)
					return
				}
			}
		}
		bucket.Objects = append(bucket.Objects[:index],bucket.Objects[index+1:]...)
		err = bucketdao.Update(bucket)
		if err != nil {
			http.Error(w, "InvalidObjectRemoval", http.StatusBadRequest)
			return
		}
	}else{
		http.Error(w, "InvalidPartNumber", http.StatusBadRequest)
		return
	}
	return
}





/*
do checking
create the byte array of the size of either the specified range or the entire object
read from each file and fille the byte array of the corresponding path
write the byte array into the response body and return
*/
func DownloadObjectEndPoint(w http.ResponseWriter, r *http.Request){
	var isHead bool
	if r.Method == "HEAD" {
		isHead = true
	}else{
		isHead = false
	}
	_,_,_,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	log.Print("initial check")
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	log.Print("complete check")
	if !object.CompleteStatus {
		http.Error(w, "NotCompletedObject", http.StatusNotFound)
		return
	}

	log.Print("range check")
	rangeheader := r.Header.Get("Range")
	ranges := [] [] int {}
	log.Print("range extract")
	ranges,err = ExtractRanges(rangeheader)
	//if rangeheader == "" {
	//
	//}else{
	//	ranges = append(ranges, []int{})
	//}
	log.Print("range extract error check")
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	log.Print("etag error check")
	etag := r.Header.Get("eTag")
	if etag != "" && object.ETag != etag {
		http.Error(w, "DifferentETag", http.StatusNotFound)
		return
	}

	//TO BE CHANGED TO ACCOMODATE MANY RANGES
	//if brange {
	//
	//}
	log.Print("getting start byte of the file and the end byte")
	startbyte := 0
	endbyte := 0
	brange := ranges[0]
	if len(brange) == 0 {
		// download all file
		startbyte = 0
		endbyte = object.Size
	} else if len(brange) == 1 {
		// download from that number till end
		startbyte= brange[0]
		endbyte = object.Size
	}else{
		startbyte = brange[0]
		if brange[1] >= object.Size {
			endbyte = object.Size
		}else{
			endbyte = brange[1]
		}
	}

	if endbyte <= startbyte {
		http.Error(w, "InvalidRange", http.StatusNotFound)
		return
	}

	if startbyte > object.Size {
		http.Error(w, "InvalidRange", http.StatusNotFound)
		return
	}


	//old hashing
	//hasher := md5.New()
	//err = WriteFileIntoWriter(object, hasher, startbyte, endbyte)
	//if err != nil {
	//	http.Error(w, err.Error(), http.StatusNotFound)
	//	return
	//}
	//hashed := HashMD5(hasher)

	w.Header().Set("eTag",object.ETag)
	w.Header().Set("Content-Type", object.ContentType)
	if !isHead {
		err = WriteFileIntoWriter(object, w, startbyte, endbyte)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
	}


}





/*
Simple, very similar to uploading parts
Do simple checking for objects
Create the metadata file
Copy the content into that file
Update the object and bucket and done
*/
func AddUpdateObjectMetadataEndpoint(w http.ResponseWriter, r *http.Request){
	bucketname,objectname,bucket,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	//get key
	key := r.URL.Query().Get("key")
	if len(key) == 0 {
		return
	}
	//get val
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	val := string(body)

	// check if meta data exist and write to it if it does
	found := false
	for i, metadata := range object.Attributes {
		if metadata.Key == key {
			found = true
			object.Attributes[i].Value = val
			if key == "Content-Type" {
				object.ContentType = val
			}
			break
		}
	}

	// otherwise create metadata
	if !found {
		nmeta := MetaData{
			ID: bson.NewObjectId(),
			ObjectName: objectname,
			BucketName: bucketname,
			Key: key,
			Value: val,
		}
		object.Attributes = append(object.Attributes, nmeta)
	}

	// upaditng bucket since we changed the object inside it
	bucketdao.UpdateObject(bucket,object)
}

func GetObjectMetadataByKeyEndPoint(w http.ResponseWriter, r *http.Request){
	_,_,_,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	//get key
	key := r.URL.Query().Get("key")
	if len(key) == 0 {
		return
	}

	mmap := map[string]string {}

	// search for map, map would be empty anw so just return that map
	for _,metadata := range object.Attributes {
		if metadata.Key == key {
			mmap[key] = metadata.Value
			break
		}
	}
	RespondWithJson(w,http.StatusOK,mmap)
}


func GetAllMetadataEndPoint(w http.ResponseWriter, r *http.Request){
	_,_,_,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	mmap := map[string]string {}
	// search for map, map would be empty anw so just return that map
	for _,metadata := range object.Attributes {
		mmap[metadata.Key] = metadata.Value
	}
	RespondWithJson(w,http.StatusOK,mmap)
}

func DeleteMetadataKeyEndpoint(w http.ResponseWriter, r *http.Request){
	_,_,bucket,object,err := CheckIfBucketAndObjectExists(bucketdao,r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	//get key
	key := r.URL.Query().Get("key")
	if len(key) == 0 {
		return
	}
	index := -1
	for i,metadata := range object.Attributes {
		if metadata.Key == key {
			index = i
			if key == "Content-Type" {
				object.ContentType = ""
			}
			break
		}
	}
	if index != -1 {
		object.Attributes = append(object.Attributes[:index],object.Attributes[index+1:]...)
	}
	bucketdao.UpdateObject(bucket,object)
}

func GetEndPoints(w http.ResponseWriter, r *http.Request) {
	if strings.Contains(r.URL.RawQuery,"metadata") {
		splitted := strings.Split(r.URL.RawQuery,"&")
		if len(splitted) == 1 {
			GetAllMetadataEndPoint(w,r)
		}else if len(splitted) == 2 {
			GetObjectMetadataByKeyEndPoint(w,r)
		}else {
			http.Error(w, "BadRouting", http.StatusNotFound)
			return
		}
	} else{
		DownloadObjectEndPoint(w,r)
	}
}

const (
	bucketPath = `/{bucketName}`
	objectPath = `/{objectName}`
)
func main() {
	router := mux.NewRouter()
	var buckObj = fmt.Sprintf("%s%s",bucketPath,objectPath)

	/* Bucket Management */
	router.HandleFunc(bucketPath, CreateBucketEndPoint).Queries("create","{create}").Methods("POST","OPTIONS")
	router.HandleFunc(bucketPath, DropBucketEndPoint).Queries("delete","{delete}").Methods("DELETE","OPTIONS")
	router.HandleFunc(bucketPath, ListObjectsInBucketEndPoint).Queries("list","{list}").Methods("GET","OPTIONS")

	/* Object Management */
	router.HandleFunc(buckObj, CreateMultiPartsUploadTicketEndPoint).Queries("create","{create}").Methods("POST","OPTIONS")
	router.HandleFunc(buckObj, UploadObjectPartsEndPoint).Queries("partNumber","{partNumber}").Methods("PUT","OPTIONS")
	router.HandleFunc(buckObj, CompleteMultiPartUploadEndPoint).Queries("complete","{complete}").Methods("POST","OPTIONS")
	router.HandleFunc(buckObj, DeletePartEndPoint).Queries("delete","{delete}","partNumber","{partNumber}").Methods("DELETE","OPTIONS")
	router.HandleFunc(buckObj, DeleteObjectEndPoint).Queries("delete","{delete}").Methods("DELETE","OPTIONS")

	router.HandleFunc(buckObj, AddUpdateObjectMetadataEndpoint).Queries("metadata","{metadata}","key","{key}").Methods("PUT","OPTIONS")
	router.HandleFunc(buckObj, DeleteMetadataKeyEndpoint).Queries("metadata","{metadata}","key","{key}").Methods("DELETE","OPTIONS")
	router.HandleFunc(buckObj, GetEndPoints).Queries("metadata","{metadata}","key","{key}").Methods("GET","OPTIONS")
	router.HandleFunc(buckObj, GetEndPoints).Queries("metadata","{metadata}").Methods("GET","OPTIONS")
	router.HandleFunc(buckObj, GetEndPoints).Methods("GET","OPTIONS")
	router.HandleFunc(buckObj, GetEndPoints).Methods("HEAD","OPTIONS")

	allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With"})
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	r2 := handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods) (router)
	log.Println(serverconfig.Server + ":" + strconv.Itoa(serverconfig.Port))
	srv := &http.Server{
		Handler:      handlers.LoggingHandler(os.Stdout, r2),
		Addr:         serverconfig.Server + ":" + strconv.Itoa(serverconfig.Port),
		WriteTimeout: 300 * time.Second,
		ReadTimeout:  300 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}


