package config

import (
	"log"

	"github.com/BurntSushi/toml"
)

// Represents database server and credentials
type Config struct {
	Productiondatabaseserver string
	Productiondatabase string
	Productiondatabaseport int
	Productionserver string
	Productionserverport int
	Devdatabaseserver string
	Devdatabase string
	Devdatabaseport int
	Devserver string
	Devserverport int
}

// Represents Current Server in used
type SOSConfig struct {
	Server string
	Port int
	Type string
}

// Read and parse the configuration file
func (c *Config) Read() {
	if _, err := toml.DecodeFile("config.toml", &c); err != nil {
		log.Fatal(err)
	}
}