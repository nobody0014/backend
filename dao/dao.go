package dao

import (
    "errors"
    "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"
    "log"
    . "p0/model"
    "time"
)


type BucketDAO struct {
    Server   string
    Database string
    Port int
}



func(b *BucketDAO) FindAll()([] Bucket, error){
    var buckets[] Bucket
    err := db.C(COLLECTION).Find(bson.M {}).All(&buckets)
    return buckets, err
}

func(b *BucketDAO) FindAllObjectsInBucket(bucketname string)([] Bucket, error){
    var buckets[] Bucket
    err := db.C(COLLECTION).Find(bson.M {"name": bucketname}).All(&buckets)
    return buckets, err
}

func(b *BucketDAO) FindById(id string)(Bucket, error){
    var bucket Bucket
    err := db.C(COLLECTION).Find(bson.ObjectIdHex(id)).One(&bucket)
    return bucket, err
}

func(b *BucketDAO) FindByName(name string)(Bucket, error){
    var bucket Bucket
    err := db.C(COLLECTION).Find(bson.M{"name": name}).One(&bucket)
    if err != nil {
        return Bucket{}, err
    }
    return bucket, errors.New("BucketExist")
}

func(b *BucketDAO) Insert(bucket Bucket) error{
    err := db.C(COLLECTION).Insert(&bucket)
    return err
}

func(b *BucketDAO) Delete(bucket Bucket) error{
    err := db.C(COLLECTION).Remove(&bucket)
    return err
}

func(b *BucketDAO) Update(bucket Bucket) error{
    bucket.Modified = time.Now().Unix()
    err := db.C(COLLECTION).UpdateId(bucket.ID, &bucket)
    return err
}

func(b *BucketDAO) UpdateObject(bucket Bucket, object Object) error{
    bucket.Modified = time.Now().Unix()
    replaceIndex := -1
    for i,obj := range bucket.Objects {
        if obj.Name == object.Name {
            replaceIndex = i
        }
    }
    bucket.Objects[replaceIndex] = object
    err := db.C(COLLECTION).UpdateId(bucket.ID, &bucket)
    return err
}


var db *mgo.Database

const (
    COLLECTION = "buckets"
)

func (m *BucketDAO) Connect(){
    //println(m.Server)
    //println(m.Port)
    //path := m.Server + strconv.Itoa(serverconfig.Port)

    var err error = nil
    var session *mgo.Session
    for counter := 0; counter < 10; counter++{
        session, err = mgo.Dial(m.Server)
        if err == nil {
            break
        }
        time.Sleep(5 * time.Second)
    }
    if err != nil {
        log.Fatal(err)
    }
    db = session.DB(m.Database)
}