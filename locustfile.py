from locust import HttpLocust, TaskSet, task
import numpy as np
import string
import random
import hashlib
import os


def create_random_string(n):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def get_file_size(fname):
    return os.path.getsize(fname)


available_files = ["application_test7","application_test3"]
file_mapping = {
    "application_test7": [
        "./testdata/application_test.csv--aa",
        "./testdata/application_test.csv--ab",
        "./testdata/application_test.csv--ac",
        "./testdata/application_test.csv--ad",
        "./testdata/application_test.csv--ad",
        "./testdata/application_test.csv--ae",
        "./testdata/application_test.csv--af"],
    "application_test3": [
        "./testdata/application_test.csv-aa",
        "./testdata/application_test.csv-ab",
        "./testdata/application_test.csv-ac"
    ]}


class UserBehavior(TaskSet):

    def __init__(self, parent):
        TaskSet.__init__(self, parent)
        self.bucketname = create_random_string(10)
        self.objects = []

    def on_start(self):
        self.client.post("/"+self.bucketname+"?create")
        for file in available_files:
            nobject_name = create_random_string(10)
            while (nobject_name in self.objects):
                nobject_name = create_random_string(10)
            self.client.post("/" + self.bucketname + "/" + nobject_name + "?create")
            for i in range(len(file_mapping[file])):
                headers = {"Content-Length": str(get_file_size(file_mapping[file][i])),
                           "Content-MD5": str(md5(file_mapping[file][i]))}
                self.client.put("/" + self.bucketname + "/" + nobject_name + "?partNumber=" +
                                str(i + 1), data=open(file_mapping[file][i], 'rb'), headers=headers)
            self.client.post("/" + self.bucketname + "/" + nobject_name + "?complete")
            self.objects.append(nobject_name)

    def on_stop(self):
        self.client.delete("/"+self.bucketname+"?delete")

    @task(5)
    def tag_metadata(self):
        if len(self.objects) > 0:
            key = create_random_string(4)
            val = create_random_string(8)
            object = self.objects[np.random.randint(0, len(self.objects))]
            self.client.put("/" + self.bucketname + "/" + object + "?metadata&" + "key=" + key, data=val)

    @task(10)
    def get_metadata(self):
        if len(self.objects) > 0:
            object = self.objects[np.random.randint(0, len(self.objects))]
            self.client.get("/" + self.bucketname + "/" + object + "?metadata")


    @task(1)
    def download_object(self):
        if len(self.objects) > 0:
            object = self.objects[np.random.randint(0, len(self.objects))]
            self.client.get("/" + self.bucketname + "/" + object)


class StorageServer(HttpLocust):
    task_set = UserBehavior
    min_wait = 500
    max_wait = 1500