package misc

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"hash"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
	. "p0/model"
	. "p0/dao"
)

//func CreateTempFile(bucketname string,object string,startbyte int,endbyte int) (io.Writer){
//	file,err := os.Stat("-temp-" + strconv.Itoa(startbyte) + "-" + strconv.Itoa(endbyte))
//	//
//	if os.IsNotExist(err) {
//		log.Print("Deleting file")
//		err = DeleteFile(filepath)
//		if err !=nil {
//			log.Print("Error: "+err.Error())
//			return err
//		}
//	}// no else cus u just create the file you idiot
//}

//write file into writer
func WriteFileIntoWriter(object Object,towrite io.Writer,startbyte int,endbyte int) (error){
	startloc := 0
	endloc := 0
	//we ensured that the parts are sorted accordingly so it should be fine
	sofar := 0
	//pipeR, pipeW := io.Pipe()
	//io.Co
	log.Print("downloading")
	log.Print(strconv.Itoa(startbyte) + " " + strconv.Itoa(endbyte))
	for _,part := range object.Parts {
		read := false
		cstart := startbyte
		cend := endbyte
		log.Println(strconv.Itoa(cstart) + " " + strconv.Itoa(cend))
		log.Println(part.PartByteStart, part.PartByteEnd)
		if part.PartByteStart >= startbyte {
			cstart = part.PartByteStart
			if part.PartByteEnd <= endbyte{
				cend = part.PartByteEnd
				read = true
			}else if part.PartByteEnd >= endbyte{
				cend = endbyte+1
				read = true
			}
			startloc = cstart - sofar
			endloc = cend - sofar
		}
		if part.PartByteStart <= startbyte {
			cstart = startbyte
			if part.PartByteEnd <= endbyte{
				cend = part.PartByteEnd
				read = true
			}else if part.PartByteEnd >= endbyte{
				cend = endbyte+1
				read = true
			}
			startloc = cstart - sofar
			endloc = cend - sofar
		}
		sofar = part.PartByteEnd
		log.Println(strconv.Itoa(startloc) + " " + strconv.Itoa(endloc))
		if read && endloc > startloc {
			//filepath := "./buckets/"+
			err := GetByteRangeFromSpecifiedFile(part.PartName,towrite,startloc,endloc)
			if err != nil {
				return err
			}
		}
	}
	return nil
}


func GetByteRangeFromSpecifiedFile(filename string, pipew io.Writer, readStart int, readEnd int) (error){
	file,err := os.Open(filename)
	log.Println(file.Name())
	if err != nil{
		log.Print("Error: "+err.Error())
		return  err
	}
	stat, err := file.Stat()
	if err != nil{
		log.Print("Error: "+err.Error())
		return err
	}
	log.Println(stat.Name(),stat.Mode(),stat.Size())
	defer file.Close()
	log.Println(readStart,readEnd)
	file.Seek(int64(readStart),0)
	_,err =  io.CopyN(pipew,file,int64(readEnd-readStart))
	if err != nil {
		log.Println("COPY ERROR:",err.Error())
		return err
	}
	log.Println("copied")
	return nil
}


///*
//Basically given a part range write those part into response writer
//*/
//func DownloadObjectRange(w http.ResponseWriter, startbyte int, endbyte int) {
//
//}


func GetEpoch() int64{
	return time.Now().Unix()
}

var objectnamecheck = regexp.MustCompile("(^[-_a-zA-Z0-9]{1}[-._a-zA-Z0-9]*[-_a-zA-Z0-9]{1}$|^[-_a-zA-Z0-9]{1}$)")
var bucketnamecheck = regexp.MustCompile("^[-_a-zA-Z0-9]+$")


func GetBucketName(r *http.Request) (string, error){
	splitted := strings.Split(r.URL.Path, "/")
	if len(splitted) < 1 {
		err := errors.New("NoBucketnameGiven")
		log.Print("Error: "+err.Error())
		return "", err
	}else if len(splitted) > 3 {
		err := errors.New("NoExtra'/'Allowed-Atmost2")
		log.Print("Error: "+err.Error())
		return "", err
	}
	if bucketnamecheck.MatchString(splitted[1]) {
		return splitted[1], nil
	}else{
		return "",errors.New("BadBucketname")
	}
}

/*
can be anything between 1-10,000 inclusive. {objectName} is the name of the object. objectName is case insensitive.
To simplify the matters, there is no concepts of folder or nested. Object name should be alpha-numeric
and may contains dot (.) or underscore (_) or or hyphen (-), but cannot begin or end with dot
*/
func GetObjectName(r *http.Request) (string, error){
	splitted := strings.Split(r.URL.Path, "/")
	if len(splitted) < 2 {
		err := errors.New("NoObjectnameGiven")
		log.Print("Error: "+err.Error())
		return "", err
	}else if len(splitted) > 3 {
		err := errors.New("NoExtra'/'Allowed-Atmost2")
		log.Print("Error: "+err.Error())
		return "", err
	}
	if objectnamecheck.MatchString(splitted[2]) {
		return splitted[2], nil
	}else{
		return "",errors.New("BadObjectname")
	}
}

/*
Also check for negative range and return that error
return all the ranges in int
return all the
*/
func ExtractRanges(rangeheader string) ([] []int, error){
	rexrange := regexp.MustCompile("((-?[0-9]+)-(-?[0-9]+){0,1})")
	brange := rexrange.FindAllString(rangeheader,-1)
	toreturn := [] []int {}
	if len(brange) == 0 {
		return append(toreturn,[]int{}),nil
	}
	for _,each := range brange{
		if !CheckGoodLength(each) {
			err := errors.New("InvalidRange")
			log.Print("Error: "+err.Error())
			return nil,err
		}else{
			strrange := strings.Split(each,"-")
			intrange := [] int{}
			for _,str := range strrange{
				if len(str) == 0 {
					continue
				}
				ir, err := strconv.Atoi(str)
				if err != nil {
					log.Print("Error: "+err.Error())
					return nil,err
				}
				intrange = append(intrange,ir)
			}
			toreturn = append(toreturn, intrange)
		}
	}
	return  toreturn,nil
}

// return true if positive
// return false if negative
func CheckGoodLength(rang string) bool{
	return len(strings.Split(rang,"-")) == 2
}

//Open the passed argument and check for any error
//Tell the program to call the following function when the current function returns
//Open a new hash interface to write to
//Copy the file in the hash interface and check for any error
//Get the 16 bytes hash
//Convert the bytes to a string
func HashFileMD5(filePath string) (string, error) {
	var returnMD5String string
	file, err := os.Open(filePath)
	if err != nil {
		log.Print("Error: "+err.Error())
		return returnMD5String, err
	}
	defer file.Close()
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		log.Print("Error: "+err.Error())
		return returnMD5String, err
	}
	hashInBytes := hash.Sum(nil)[:16]
	returnMD5String = hex.EncodeToString(hashInBytes)
	return returnMD5String, nil
}


//given hasher return md5
func HashMD5(hash hash.Hash) string {
	var returnMD5String string
	hashInBytes := hash.Sum(nil)[:16]
	returnMD5String = hex.EncodeToString(hashInBytes)
	return returnMD5String
}


func CreateFile(filepath string) error{
	_, err := os.Stat(filepath)
	// create file if not exists
	if !os.IsNotExist(err) {
		log.Print("Deleting file")
		err = DeleteFile(filepath)
		if err !=nil {
			log.Print("Error: "+err.Error())
			return err
		}
	}// no else cus u just create the file you idiot
	log.Print("Creating file")
	file, err := os.Create(filepath)
	if err != nil {
		log.Print("Error: "+err.Error())
		return err
	}
	file.Close()
	return nil
}

func DeleteFile(filepath string) error {
	// delete file
	err := os.Remove(filepath)
	if err != nil {
		log.Print("Error: "+err.Error())
		return err
	}
	return nil
}


/*
Basically check the follow
bucketname valid
objectname valid [object = ticket in this case]
bucket exists
object/ticket exists in that bucket and is not flagged completed
*/
func CheckIfBucketAndObjectExists(bucketdao BucketDAO, r *http.Request) (string,string,Bucket,Object,error){
	bucketname, err := GetBucketName(r)
	if err != nil {
		log.Print("Error: "+err.Error())
		return "","",Bucket{},Object{},err
	}
	objectname, err := GetObjectName(r)
	if err != nil {
		log.Print("Error: "+err.Error())
		return bucketname,objectname,Bucket{},Object{},err
	}
	bucket, err := bucketdao.FindByName(bucketname)
	if err.Error() != "BucketExist"{
		log.Print("Error: "+err.Error())
		return bucketname,objectname,bucket,Object{},err
	}
	for _, object := range bucket.Objects {
		if object.Name == objectname {
			return bucketname,objectname,bucket,object,nil
		}
	}
	return bucketname,objectname,bucket,Object{},errors.New("Object does not exists")
}




func RespondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	log.Print("Setting response header")
	w.Header().Set("Content-Type", "application/json")
	log.Print("Marshalling paylod")
	response, err := json.Marshal(payload)

	if err != nil{
		log.Print("Check error")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	log.Print("Write Header")
	w.WriteHeader(code)
	log.Print("write response")
	w.Write(response)
}


// current problem: what if the first part has less than 512 size
func DetectContentType(object Object) (string,error) {
	f, err := os.Open(object.Parts[0].PartName)
	if err != nil {
		return "", err
	}
	defer f.Close()
	buffer := make([]byte, 512)

	_, err = f.Read(buffer)
	if err != nil {
		return "", err
	}
	return http.DetectContentType(buffer), nil
}
