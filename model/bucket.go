package model

import "gopkg.in/mgo.v2/bson"

type Bucket struct {
	ID bson.ObjectId `bson:"_id" json:"id"`
	Name string `bson:"name" json:"name"`
	Created int64 `bson:"created" json:"created"`
	Modified int64 `bson:"modified" json:"modified"`
	Objects []Object `bson:"objects" json:"objects"`
}
