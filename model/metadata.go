package model

import "gopkg.in/mgo.v2/bson"

type MetaData struct{
	ID bson.ObjectId `bson:"_id" json:"id"`
	ObjectName string `bson:"objectname" json:"objectname"`
	BucketName string `bson:"bucketname" json:"bucketname"`
	Key string `bson:"key" json:"key"`
	Value string `bson:"value" json:"value"`
}
