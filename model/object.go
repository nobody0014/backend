package model

import "gopkg.in/mgo.v2/bson"

type Object struct {
	ID bson.ObjectId `bson:"_id" json:"id"`
	Name string `bson:"name" json:"name"`
	BucketName string `bson:"bucketname" json:"bucketname"`
	CompleteStatus bool `bson:"completestatus" json:"completestatus"`
	Parts [] Part `bson:"parts" json:"parts"`
	ETag string `bson:"eTag" json:"eTag"`
	Size int `bson:"size" json:"size"`
	Attributes [] MetaData `bson:"attributes" json:"attributes"`
	Created int64 `bson:"created" json:"created"`
	Modified int64 `bson:"modified" json:"modified"`
	ContentType string `bson:"content-type" json:"content-type"`
}