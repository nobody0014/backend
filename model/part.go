package model

import "gopkg.in/mgo.v2/bson"

type Part struct{
	ID bson.ObjectId `bson:"_id" json:"id"`
	ObjectName string `bson:"objectname" json:"objectname"`
	BucketName string `bson:"bucketname" json:"bucketname"`
	PartName string `bson:"partname" json:"partname"`
	PartNumber int `bson:"partnumber" json:"partnumber"`
	PartByteStart int `bson:"partbytestart" json:"partbytestart"`
	PartByteEnd int `bson:"partbyteend" json:"partbyteend"`
	PartSize int `bson:"partsize" json:"partsize"`
	PartMD5 string `bson:"PartMD5" json:"PartMD5"`
	UploadStatus bool `bson:"uploadstatus" json:"uploadstatus"`
}
