package model

import (
	"bytes"
	"net/http"
)

type Request struct {
	Bucketname  string
	Objectname string
	ExpectedStatusCode int
	Method string
	Query map[string]string
	Endpoint func(w http.ResponseWriter, r *http.Request)
	Headers map[string]string
	Body *bytes.Buffer
}
