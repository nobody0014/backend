package model


type CreateBucketResponse struct {
	Created   int64 `json:"created"`
	Modified int64 `json:"modified"`
	Name string `json:"name"`
}

type ListObjectsInBucketResponse struct {
	Created   int64 `json:"created"`
	Modified int64 `json:"modified"`
	Name string `json:"name"`
	Objects []ReturnObject `json:"objects"`
}

type UploadObjectPartsResponse struct {
	MD5 string `json:"md5"`
	Length int `json:"length"`
	PartNumber int `json:"partNumber"`
	Error string `json:"error"`
}

type CompleteObjectResponse struct {
	ETag string `json:"eTag"`
	Length int `json:"length"`
	Name string `json:"name"`
	Error string `json:"error"`
}

type ReturnObject struct {
	Name string `json:"name"`
	ETag string `json:"eTag"`
	Created int64 `json:"created"`
	Modified int64 `json:"modified"`
}
