package main

import (
	"bytes"
	"io"
	"log"
	"os"
	"strconv"
	. "p0/model"
	. "p0/misc"
)

func CreateBucketRequest(expectedstatuscode int,bucketname string) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: "",
		ExpectedStatusCode: expectedstatuscode,
		Method: "POST",
		Query: map[string]string{"create": ""},
		Endpoint: CreateBucketEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func DeleteBucketRequest(expectedstatuscode int,bucketname string) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: "",
		ExpectedStatusCode: expectedstatuscode,
		Method: "DELETE",
		Query: map[string]string{"delete": ""},
		Endpoint: DropBucketEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func ListBucketRequest(expectedstatuscode int,bucketname string) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: "",
		ExpectedStatusCode: expectedstatuscode,
		Method: "GET",
		Query: map[string]string{"list": ""},
		Endpoint: ListObjectsInBucketEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func CreateObjectRequest(expectedstatuscode int,bucketname string,objectname string) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "POST",
		Query: map[string]string{"create": ""},
		Endpoint: CreateMultiPartsUploadTicketEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func UploadPartsObjectRequest(expectedstatuscode int,bucketname string,objectname string,partnumber int,filepath string) Request {
	md5,err := HashFileMD5(filepath)
	if err != nil {
		log.Fatalln(err.Error())
	}
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatalln(err.Error())
	}
	stat, err := os.Stat(filepath)
	if err != nil {
		log.Fatal(err.Error())
	}
	filesize  := strconv.Itoa(int(stat.Size()))
	body := &bytes.Buffer{}
	io.Copy(body, file)
	file.Close()
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "PUT",
		Query: map[string]string{"partNumber": strconv.Itoa(partnumber)},
		Endpoint: UploadObjectPartsEndPoint,
		Headers: map[string]string{"Content-MD5":md5,"Content-Length": filesize},
		Body: body,
	}
}

func UploadStringPartsObjectRequest(expectedstatuscode int,bucketname string,objectname string,partnumber string,filepath string) Request {
	md5,err := HashFileMD5(filepath)
	if err != nil {
		log.Fatalln(err.Error())
	}
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatalln(err.Error())
	}
	stat, err := os.Stat(filepath)
	if err != nil {
		log.Fatal(err.Error())
	}
	filesize  := strconv.Itoa(int(stat.Size()))
	body := &bytes.Buffer{}
	//writer := multipart.NewWriter(body)
	//part, err := writer.CreateFormFile("binary",filepath)
	//if err != nil {
	//	log.Fatal(err)
	//}
	io.Copy(body, file)
	file.Close()
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "PUT",
		Query: map[string]string{"partNumber": partnumber},
		Endpoint: UploadObjectPartsEndPoint,
		Headers: map[string]string{"Content-MD5":md5,"Content-Length": filesize},
		Body: body,
	}
}

func CompleteUploadObjectRequest(expectedstatuscode int,bucketname string,objectname string) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "POST",
		Query: map[string]string{"complete":""},
		Endpoint: CompleteMultiPartUploadEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func DeletePartObjectRequest(expectedstatuscode int,bucketname string,objectname string,partnumber int) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "DELETE",
		Query: map[string]string{"partNumber":strconv.Itoa(partnumber)},
		Endpoint: DeletePartEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func DeleteStringPartObjectRequest(expectedstatuscode int,bucketname string,objectname string,partnumber string) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "DELETE",
		Query: map[string]string{"partNumber":partnumber},
		Endpoint: DeletePartEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func DeleteObjectRequest(expectedstatuscode int,bucketname string,objectname string,) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "POST",
		Query: map[string]string{},
		Endpoint: DeleteObjectEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}


func DownloadObjectRequest(expectedstatuscode int,bucketname string,objectname string,ranges map[int][]int) Request {
	headers := map[string]string{}
	if len(ranges) > 0 {
		headers["Ranges"] = "bytes="
		for key := range ranges{
			if len(ranges[key]) == 1 {
				headers["Ranges"] += strconv.Itoa(ranges[key][0])+"-"
			}else if len(ranges[key]) == 2 {
				headers["Ranges"] += strconv.Itoa(ranges[key][0])+"-"+strconv.Itoa(ranges[key][1])
			}
			headers["Ranges"] += ","
		}
		headers["Ranges"] = headers["Ranges"][:len(headers["Ranges"])-1]
	}
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "GET",
		Query: map[string]string{},
		Endpoint: DownloadObjectEndPoint,
		Headers: headers,
		Body: nil,
	}
}

func PutMetadataRequest(expectedstatuscode int,bucketname string,objectname string,key string,value string) Request {
	body := &bytes.Buffer{}
	body.Write([]byte(value))
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "PUT",
		Query: map[string]string {"metadata":"","key":key},
		Endpoint: AddUpdateObjectMetadataEndpoint,
		Headers: map[string]string{},
		Body: body,
	}
}

func DeleteMetadataKeyRequest(expectedstatuscode int,bucketname string,objectname string,key string) Request {
	return Request{
		Bucketname:         bucketname,
		Objectname:         objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method:             "DELETE",
		Query:              map[string]string {"metadata":"","key":key},
		Endpoint:           DeleteMetadataKeyEndpoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func GetMetadataWithKeyRequest(expectedstatuscode int,bucketname string,objectname string,key string) Request {
	return Request{
		Bucketname:         bucketname,
		Objectname:         objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method:             "GET",
		Query:              map[string]string {"metadata":"","key":key},
		Endpoint:           GetObjectMetadataByKeyEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}

func GetAllMetadataRequest(expectedstatuscode int,bucketname string,objectname string) Request {
	return Request{
		Bucketname: bucketname,
		Objectname: objectname,
		ExpectedStatusCode: expectedstatuscode,
		Method: "GET",
		Query: map[string]string{"metadata":""},
		Endpoint: GetAllMetadataEndPoint,
		Headers: map[string]string{},
		Body: nil,
	}
}